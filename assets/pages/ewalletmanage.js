
!function ($) {
    "use strict";

    var Ewalletmanage = function () {


    };
    
    Ewalletmanage.prototype.add = function (url) {
      			
      				$('[name=formAdd]').submit(function(){
                                       
                        $('#btn_add').attr('disabled',true);
                					 $('#btn_add').html('<span class="fa fa-spin fa-spinner"> </span> Processing .. ');
      						$.ajax({
									type: "POST",
									url: 'http://[::1]/adminpanel/wallet/addfund',
									data: 	{  
  													'user_id' 	: 	$('[name=user_id]').val(),
                                                    'amount'	: 	$('[name=amount]').val(),
                                                    'remark'	: 	$('[name=remark]').val(),
                                    },							
									cache: false,
									success: function(data){
                                            var obj = JSON.parse(data);		
                                              console.log(data);
                                                new PNotify({
                                                    title: obj[0].title,
                                                    text: obj[0].text,
                                                    type: obj[0].type,
                                                    styling: 'bootstrap3'
                                                });
                                                if(obj[0].type =='success') {
                                                    $('#balance').html("<span>"+ obj[0].new_balance +"</span>")
                                                    $('#balances').html("<span>"+ obj[0].new_balance +"</span>")
                                               
                                                }
                                                $('#btn_add').attr('disabled',false);
                                                $('#btn_add').html('Submit');
									}
								});
      					event.preventDefault();
      				});			
      },
      Ewalletmanage.prototype.deduct = function () {
      			
        $('[name=formDeduct]').submit(function(){
                       $('#btn_deduct').attr('disabled',true);
                       $('#btn_deduct').html('<span class="fa fa-spin fa-spinner"> </span> Processing .. ');
                $.ajax({
                      type: "POST",
                      url: 'http://[::1]/adminpanel/wallet/deductfund',
                      data: 	{  
                                     'user_id' 	: 	$('[name=userid]').val(),
                                      'amount'	: 	$('[name=amounts]').val(),
                                      'remark'	: 	$('[name=remarks]').val(),
                      },                
                      cache: false,
                      success: function(data){
                              var obj = JSON.parse(data);		
                                console.log(data);
                                  new PNotify({
                                      title: obj[0].title,
                                      text: obj[0].text,
                                      type: obj[0].type,
                                      styling: 'bootstrap3'
                                  });

                                  if(obj[0].type =='success') {
                                      $('#balances').html("<span>"+ obj[0].new_balance +"</span>")
                                      $('#balance').html("<span>"+ obj[0].new_balance +"</span>")
                                  }

                                  $('#btn_deduct').attr('disabled',false);
                                  $('#btn_deduct').html('Submit');
                      }
                  });
            event.preventDefault();
        });			
},
     Ewalletmanage.prototype.init = function () {  	
      		  		
                        this.add($('[name=url]').val()); 
                        this.deduct();	  		
      }
    $.Ewalletmanage = new Ewalletmanage, $.Ewalletmanage.Constructor = Ewalletmanage

}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Ewalletmanage.init();
    }(window.jQuery);



!function ($) {
    "use strict";

    var Profile = function () {


    };
    
    Profile.prototype.getUpdateProfile = function () {
      					

      				$('[name=profileForm]').submit(function(e){
      							
      							$('#btn_profile').html('<span><i class="fa fa-spin fa-spinner"></i></span>');
      							$('#btn_profile').attr('disabled',true);
			     				$.ajax({
												type: "POST",
												url: 'http://[::1]/adminpanel/member/updateinfo',
												data: $('[name=profileForm]').serialize(),
												cache: false,
												success: function(result){
													
														var obj = JSON.parse(result)
															


														new PNotify({
									                                  title: obj[0].title,
									                                  text: obj[0].text,
									                                  type: obj[0].type,
									                                  styling: 'bootstrap3'
									                              });


														$('#btn_profile').html('<span></span>Update');
														$('#btn_profile').attr('disabled',false);

														
												}
											});	


											e.preventDefault();

     						
     					})
      },

      Profile.prototype.getUpdateBank = function () {
      			
      				$('[name=bankForm]').submit(function(e){	

      							$('#btn_bank').html('<span><i class="fa fa-spin fa-spinner"></i></span>');
      							$('#btn_bank').attr('disabled',true);
		     					$.ajax({
											type: "POST",
											url: 'http://[::1]/adminpanel/member/updatebank',
											data: $('[name=bankForm]').serialize(),
											cache: false,
											success: function(data){

														var obj = JSON.parse(data)
															


														new PNotify({
									                                  title: obj[0].title,
									                                  text: obj[0].text,
									                                  type: obj[0].type,
									                                  styling: 'bootstrap3'
									                              });

														$('#btn_bank').html('<span></span>Update');
														$('#btn_bank').attr('disabled',false);

														

											}
										});	

     						e.preventDefault();
     					})
      },
   
     Profile.prototype.init = function () {


      		  		this.getUpdateProfile();
      		  		this.getUpdateBank();
      		  
      		  		
      }

    $.Profile = new Profile, $.Profile.Constructor = Profile

}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Profile.init();
    }(window.jQuery);


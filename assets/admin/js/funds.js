      var table ; 
      var tableHistory ;    
        $(function() {
          tableHistory = $('#myTable').DataTable({
                            "ajax": 'http://[::1]/adminpanel/member/get-approvefundrequest',    
                             dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0
                        });


          table = $('#datatable-button').DataTable({
                            "ajax": 'http://[::1]/adminpanel/member/get-fundrequest',    
                             dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0
                        });

            $('#datatable-button').on('click','#paid',function(){

                     var data            = $(this).val();
                     var user_id         = $(this).attr('data-id');
                     var wallet_type     = $(this).attr('data-wallet');
                     var amount          = $(this).attr('data-amount');

                     $(this).html('<i class="fa fa-spin fa-spinner"></i> processing ');
                     $(this).attr('disabled',true);
                     
                       var formData = {
                                          'id'       : data,
                                          'user_id'  : user_id,
                                          'wallet_type' : wallet_type,
                                          'amount'  : amount
                                    };
                    $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure you want to mark as paid the request of userid : '+ user_id + ' ?',
                                buttons: {
                                    confirm: function () {
                                      
                                          
                                         $.ajax({                          
                                                  type        : 'POST', 
                                                  url         : 'http://[::1]/adminpanel/member/paid-request', 
                                                  data        : formData, 
                                                  dataType    : 'json', 
                                                  encode          : true
                                              }).done(function(data) {

                                                      $('#paid').html('<i class="fa fa-check"></i> Approve ');
                                                      $('#paid').attr('disabled',false);
                                              
                                            
                                                                    new PNotify({
                                                                                  title: data.title,
                                                                                  text: data.text,
                                                                                  type: data.type,
                                                                                  styling: 'bootstrap3'
                                                                              });


                                                                    console.log(data.text);

                                                          table.ajax.reload();
                                                          tableHistory.ajax.reload();
                                                          
                                            });
                                    },
                                    cancel: function () {
                                              
                                                              new PNotify({
                                                                              title   : 'Cancel',
                                                                              text    : 'Paid action has been cancelled',
                                                                              type    : 'success',
                                                                              styling : 'bootstrap3'
                                                                          });

                                                      $('#paid').html('<i class="fa fa-check"></i> Approve ');
                                                      $('#paid').attr('disabled',false);
                                                      table.ajax.reload();
                                                      tableHistory.ajax.reload();
                                    }
                                }
                        });
            });


          $('#datatable-button').on('click','#approve',function(){

                     var data            = $(this).val();
                     var user_id         = $(this).attr('data-id');
                     var wallet_type     = $(this).attr('data-wallet');
                     var amount          = $(this).attr('data-amount');

                     $(this).html('<i class="fa fa-spin fa-spinner"></i> processing ');
                     $(this).attr('disabled',true);
                     
                       var formData = {
                                          'id'       : data,
                                          'user_id'  : user_id,
                                           'user_id'  : user_id,
                                          'wallet_type' : wallet_type,
                                          'amount'  : amount
                                    };
                    $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure you want to set the request of userid : '+ data + ' as Waiting for approval?',
                                buttons: {
                                    confirm: function () {
                                      
      
                                         $.ajax({                          
                                                type        : 'POST', 
                                                url         : 'http://[::1]/adminpanel/member/approved-request', 
                                                data        : formData, 
                                                dataType    : 'json', 
                                                encode          : true
                                            }).done(function(data) {
                        
                                                      $('#approve').html('<i class="fa fa-check"></i> Approve ');
                                                      $('#approve').attr('disabled',false);
                                              
                                            
                                                                    new PNotify({
                                                                                  title: data.title,
                                                                                  text: data.text,
                                                                                  type: data.type,
                                                                                  styling: 'bootstrap3'
                                                                              });


                                                          table.ajax.reload();
                                                           tableHistory.ajax.reload();
                                                          
                                            });
                                    },
                                    cancel: function () {
                                              
                                                              new PNotify({
                                                                              title   : 'Cancel',
                                                                              text    : 'Approval action has been cancelled',
                                                                              type    : 'success',
                                                                              styling : 'bootstrap3'
                                                                          });

                                                      $('#approve').html('<i class="fa fa-check"></i> Approve ');
                                                       $('#approve').attr('disabled',false);
                                                       table.ajax.reload();
                                                       tableHistory.ajax.reload();
                                    }
                                }
                        });

            });

              $('#datatable-button').on('click','#cancel',function(){

                     var data            = $(this).val();
                     var user_id         = $(this).attr('data-id');
                     var wallet_type     = $(this).attr('data-wallet');
                     var amount          = $(this).attr('data-amount');

                     $(this).html('<i class="fa fa-spin fa-spinner"></i> processing ');
                     $(this).attr('disabled',true);
                     
                       var formData = {
                                          'id'       : data,
                                          'user_id'  : user_id,
                                           
                                          'wallet_type' : wallet_type,
                                          'amount'  : amount
                                    };
                    $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure you want to cancel the request of userid : '+ user_id + ' ?',
                                buttons: {
                                    confirm: function () {
                                      
                                          
                                         $.ajax({                          
                                                type        : 'POST', 
                                                url         : 'http://[::1]/adminpanel/member/cancelled-request', 
                                                data        : formData, 
                                                dataType    : 'json', 
                                                encode          : true
                                            }).done(function(data) {
                                                              

                                                              console.log(data);
                                                      $('#cancel').html('<i class="fa fa-check"></i> Cancel ');
                                                      $('#cancel').attr('disabled',false);
                                              
                                            
                                                                    new PNotify({
                                                                                  title: data.title,
                                                                                  text: data.text,
                                                                                  type: data.type,
                                                                                  styling: 'bootstrap3'
                                                                              });


                                                          table.ajax.reload();
                                                           tableHistory.ajax.reload();
                                                          
                                            });
                                    },
                                    cancel: function () {
                                              
                                                              new PNotify({
                                                                              title   : 'Cancel',
                                                                              text    : 'Cancel action has been declined',
                                                                              type    : 'success',
                                                                              styling : 'bootstrap3'
                                                                          });

                                                      $('#cancel').html('<i class="fa fa-check"></i>Cancel');
                                                       $('#cancel').attr('disabled',false);
                                                       table.ajax.reload();
                                                       tableHistory.ajax.reload();
                                    }
                                }
                        });

            });
             
             

        });

!function ($) {
    "use strict";

    var Announcement = function () {


    };
    
    Announcement.prototype.getUpdateAnnouncement = function () {
      					

      				$('[name=announcementForm]').submit(function(e){

      						
      							
      							$('#btn_announce').html('<span><i class="fa fa-spinner"></i></span>');
      							$('#btn_announce').attr('disabled',true);
			     				
			     				$.ajax({
												type: "POST",
												url: 'http://[::1]/adminpanel/member/updateannouncement',
												data: {
														'title' 	: $('[name=title]').val(),
														'content'	: $('.summernote').val()

														},
												cache: false,
												success: function(result){
													
														var obj = JSON.parse(result)
															


														new PNotify({
									                                  title: obj[0].title,
									                                  text: obj[0].text,
									                                  type: obj[0].type,
									                                  styling: 'bootstrap3'
									                              });


														$('#btn_announce').html('<span></span>Update');
														$('#btn_announce').attr('disabled',false);

														
												}
											});	


											e.preventDefault();

     						
     					});
      },

      Announcement.prototype.clearAnnouncemnet = function () {


      			$('#btn_clear').click(function(e){

      					


      							
      							$('#btn_clear').html('<span><i class="fa fa-spinner"></i></span>');
      							$('#btn_clear').attr('disabled',true);
			     				
			     				$.ajax({
												type: "POST",
												url: 'http://[::1]/adminpanel/member/clearannouncement',
												data: {
														'title' 	: $('[name=title]').val(),
														'content'	: $('.summernote').val()

														},
												cache: false,
												success: function(result){
													
														var obj = JSON.parse(result)
															


														new PNotify({
									                                  title: obj[0].title,
									                                  text: obj[0].text,
									                                  type: obj[0].type,
									                                  styling: 'bootstrap3'
									                              });


													 $('[name=title]').val('');
													 

													  $('.summernote').summernote('code', '');
														$('#btn_clear').html('<span></span>Clear');
														$('#btn_clear').attr('disabled',false);

														
												}
											});	


											e.preventDefault();

     						
     					});


      },

      
   
     Announcement.prototype.init = function () {


      		  		this.getUpdateAnnouncement();
      		  		this.clearAnnouncemnet();
      		  	
      		  
      		  		
      }

    $.Announcement = new Announcement, $.Announcement.Constructor = Announcement

}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Announcement.init();
    }(window.jQuery);


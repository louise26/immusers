      var table ; 
      var tableHistory ;    
        $(function() {
          tableHistory = $('#myTable').DataTable({
                            "ajax": 'http://[::1]/adminpanel/member/get-close',    
                             dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0
                        });


          table = $('#datatable-button').DataTable({
                            "ajax": 'http://[::1]/adminpanel/member/get-request',    
                             dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0
                        });

            $('#datatable-button').on('click','#approve',function(){

                      var data   = $(this).val();
                     var id     = $(this).attr('data');
                     $(this).html('<i class="fa fa-spin fa-spinner"></i> processing ');
                     $(this).attr('disabled',true);
                     
                       var formData = {
                                          'id'       : data,
                                    };
                    $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure you want to close userid : '+ data + ' ?',
                                buttons: {
                                    confirm: function () {
                                      
                                          
                                         $.ajax({                          
                                                type        : 'POST', 
                                                url         : 'http://[::1]/adminpanel/member/approve-request', 
                                                data        : formData, 
                                                dataType    : 'json', 
                                                encode          : true
                                            }).done(function(data) {
                        
                                                      $('#approve').html('<i class="fa fa-check"></i> Approve ');
                                                      $('#approve').attr('disabled',false);
                                              
                                            
                                                                    new PNotify({
                                                                                  title: data.title,
                                                                                  text: data.text,
                                                                                  type: data.type,
                                                                                  styling: 'bootstrap3'
                                                                              });


                                                          table.ajax.reload();
                                                           tableHistory.ajax.reload();
                                                          
                                            });
                                    },
                                    cancel: function () {
                                              
                                                              new PNotify({
                                                                              title   : 'Cancel',
                                                                              text    : 'Approval action has been cancelled',
                                                                              type    : 'success',
                                                                              styling : 'bootstrap3'
                                                                          });

                                                      $('#approve').html('<i class="fa fa-check"></i> Approve ');
                                                       $('#approve').attr('disabled',false);
                                                       table.ajax.reload();
                                                       tableHistory.ajax.reload();
                                    }
                                }
                        });

            });


            $('#datatable-button').on('click','#cancel',function(){

                     var data   = $(this).val();
                     var id     = $(this).attr('data');
                     $(this).html('<i class="fa fa-spin fa-spinner"></i> processing ');
                     $(this).attr('disabled',true);
                     
                       var formData = {
                                          'id'       : id,
                                    };
                    $.confirm({
                                title: 'Confirm!',
                                content: 'Are you sure you want to cancel request from userid : '+ data + ' ?',
                                buttons: {
                                    confirm: function () {
                                      
                              
                                         $.ajax({                          
                                                type        : 'POST', 
                                                url         : 'http://[::1]/adminpanel/member/cancel-request', 
                                                data        : formData, 
                                                dataType    : 'json', 
                                                encode          : true
                                            }).done(function(data) {
                                                  

                                                      $('#cancel').html('<i class="fa fa-times"></i> Cancel ');
                                                       $('#cancel').attr('disabled',false);
                                              
                                                      
                                                          new PNotify({
                                                                                  title: data.title,
                                                                                  text: data.text,
                                                                                  type: data.type,
                                                                                  styling: 'bootstrap3'
                                                                              });


                                                         table.ajax.reload();
                                                           tableHistory.ajax.reload();
                                                          
                                            });
                                    },
                                    cancel: function () {
                                              


                                                                    new PNotify({
                                                                                  title: 'Cancel',
                                                                                  text: 'Cancel action has been decline',
                                                                                  type: 'success',
                                                                                  styling: 'bootstrap3'
                                                                              });

                                                      $('#cancel').html('<i class="fa fa-times"></i> Cancel ');
                                                       $('#cancel').attr('disabled',false);
                                                       table.ajax.reload();
                                                           tableHistory.ajax.reload();
                                    }
                                }
                        });

            });
             

        });
<?php
defined('BASEPATH') or exit('No direct script access allowed');



class ProfitshareController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		

	}

	public function index () {
			
				if($this->is_logged_in() ) {

					$data = [
								'announcement'	=> $this->model_users->query("SELECT * from promo")->result()
							];
						
					return $this->load->view('member_management/profitshareView',$data);
				}
				else {

					redirect('login');
				}
	}


	public function getProfits(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select * from profitshare order by id desc  ")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

						

					array_push($data,[
										$i,
										$value->com_percent, 
										($value->com_percent/2),
										($value->com_percent/2),
										date('F d, Y',strtotime($value->posted_date))

									]);

						
				}


			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}



	public function addProfit() {
			$result = [] ;

			$insertCredit = [] ;
			$updateWallet = [] ;
			if( $this->is_logged_in() ) {

					


					$this->load->model('model_profitshare');

					
					$date 	= date('Y-m-d');

					$profits = [
										'com_percent' 	=>	$this->input->post('profit'),
										'posted_date'	=> $date
									];

				

					if( $this->model_profitshare->insert( $profits ) ){

						$profit 	= $this->input->post('profit') / 2;
						$date 		= date('Y-m-d');

							foreach ($this->model_lifejacket_subscription->select('user_id,amount,lifejacket_id,sponsor,remark') as $key => $value) {

										$user_id 			= 	$value->user_id;
									    $amounts 			= 	$value->amount;
									    $lifejacket_id 		=	$value->lifejacket_id;
									    $lifejacket_sponsor = 	$value->sponsor;
									    $lifejacket_remark	= 	$value->remark;

										 	if($lifejacket_remark=='Coin Purchase')
										    {
										        $newdesc=$lifejacket_sponsor." Coin";
										    }
										    else
										    {
										        $newdesc=$lifejacket_id." Package";
										    }
										    $profits=0;
										    $xc=0.10;
										    if($amounts>=5000)
										    {
										       $profits=$profit;
										    }
										    else
										    {
										       $profits=$profit;
										    }

										     	$rwallet 		= $value->amount*$profits/100;
    											$invoice_no 	= $user_id.rand(10000,99999);

    									
    									if($rwallet> 0) {

    										$chald  = $this->model_acc_close_request->count_ref(['lfjid'=>$lifejacket_id,'user_id'=>$user_id]);


    										 if($chald>0)
										        {}
										        else
										        {

										        	$ewalletbalance = 0 ;
										        	
										        	 $urls="https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];



										        	 foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$user_id]) as $key => $bal) {	
										        	 			$ewalletbalance =  $bal->amount;
										        	 	}

										        		
										        	 			 array_push($updateWallet,[
													        	 							'user_id' => $user_id,
													        	 							'amount'  => $bal->amount + $rwallet ,

													        	 						  ]);
										        		

										        	array_push($insertCredit,[

														        				'transaction_no'	=> $invoice_no,
														        				'user_id'			=> $user_id,
														        				'credit_amt'		=> $rwallet,
														        				'receiver_id'		=> $user_id,
														        				'sender_id'			=> '123456',
														        				'debit_amt'			=>0,
														        				'admin_charge'		=>0,
														        				'invoice_no'		=> $invoice_no,
														        				'receive_date'		=> $date,
														        				'ttype'				=> 'Profit Sharing Bonus',
														        				'TranDescription'	=> $newdesc,
														        				'Cause'				=> 'Commission of MYR '.$rwallet.' For Package '.$amounts,
														        				'Remark'			=> $amounts,
														        				'product_name'		=> 'Bonus From Profit',
														        				'status'			=> '0',
														        				'ewallet_used_by'	=> 'Withdrawal Wallet',
														        				'current_url'		=> $urls 
												        					]

										        					);
										        }
    									}
										
							}
									$this->updateWallet($updateWallet);
									$this->insertCredit($insertCredit);

									array_push($result ,[

															'title' 	=>	'Add Sucess',
															'text'		=> 	'Todays profit has been added',
															'type'		=>	'success'
												 		]) ;
										
						}
						else {
									array_push($result , [
															'title' 	=>'Add Failed',
															'text'		=> 'Something went wrong',
															'type'		=>'error'
														]);


									
		
							}


					

				
			
			}

			echo json_encode($result);

			

	}

	public function insertCredit($data = []){


					$this->db->insert_batch('credit_debit', $data); 

			}

			public function updateWallet($data = [] ) {


				$this->db->update_batch('final_e_wallet',$data, 'user_id'); 

			}


}
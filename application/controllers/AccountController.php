<?php
defined('BASEPATH') or exit('No direct script access allowed');



class AccountController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		

	}

	public function index () {
			
				if($this->is_logged_in() ) {

					
						
					return $this->load->view('member_management/closeAccountView');
				}
				else {

					redirect('login');
				}
	}



	public function getRequest(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select acc_close_request.id,acc_close_request.ts,acc_close_request.amount,user_registration.user_id,user_registration.username,user_registration.first_name,user_registration.last_name from acc_close_request  JOIN user_registration ON acc_close_request.user_id=BINARY user_registration.user_id where acc_close_request.status='0'")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,

									[
										$i,
										$value->user_id, 
										$value->username,
										$value->first_name . ' ' . $value->last_name,
										$value->ts,
										$value->amount,
										'<button  class="btn btn-info btn-xs" id="approve" data="'.$value->user_id.'" value="'.$value->user_id.'"><i class="fa fa-check"></i> Approve </button><button  class="btn btn-warning btn-xs" id="cancel" data="'.$value->id.'" value="'.$value->user_id.'"><i class="fa fa-times"></i> Cancel </button>'
									]);

						
				}


			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


	public function approveRequest() {

		
		if($this->is_logged_in()){

			$result = [] ;
			$id = $this->input->post('id');



			



			$result  = $this->model_acc_close_request->update(['status'=>'1'],['user_id'=>$id]);

				$this->model_users->update(['user_status'=>'1'],['user_id'=>$id]);
			if($result) {
						$result  = [

															'title' 	=>	'Close Sucess',
															'text'		=> 	'Account has been closed',
															'type'		=>	'success'
												 		] ;
			}
			else {

						$result = [

															'title' 	=>	'Close Failed',
															'text'		=> 	'Something went wrong',
															'type'		=>	'error'
												 		]  ;
			}

			echo json_encode($result);
			

		}

	}

		public function cancelRequest() {

		
		if($this->is_logged_in()){

			$result = [] ;
			$id = $this->input->post('id');



			



			$result  = $this->model_acc_close_request->delete(['id'=>$id]);

		
			if($result) {
						$result  = [

															'title' 	=>	'Cancel Sucess',
															'text'		=> 	'Request has  been canceled',
															'type'		=>	'success'
												 		] ;
			}
			else {

						$result = [

															'title' 	=>	'Cancel Failed',
															'text'		=> 	'Something went wrong',
															'type'		=>	'error'
												 		]  ;
			}

			echo json_encode($result);
			

		}


		
	}



		public function getClose(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select acc_close_request.id,acc_close_request.ts,acc_close_request.amount,user_registration.user_id,user_registration.username,user_registration.first_name,user_registration.last_name from acc_close_request  JOIN user_registration ON acc_close_request.user_id=BINARY user_registration.user_id where acc_close_request.status='1'")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;


					array_push($data,

									[
										$i,
										$value->user_id, 
										$value->username,
										$value->first_name . ' ' . $value->last_name,
										$value->ts,
										$value->amount,
										'Closed'
									]);

						
				}


			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


}

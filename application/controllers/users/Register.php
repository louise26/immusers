<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Register  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		

		


	}

	
		public function index(){
			return $this->load->view('users/register');

		}

	public function register(){

		
			
				
				return $this->load->view('users/register');



	}

	public function password_reset(){
			return $this->load->view('users/password_reset');
	}


	public function sendCode(){
		//$this->load->library('email');

		$emails  = $this->input->post('emails');
		
		$rand = rand(10000,99999);
		$code = md5($rand);
		$data = [];
		$this->session->set_userdata(['reset_code'=>$code]);
		$msg = '<!doctype html>
			<html>

			<head>
			    <meta charset="utf-8">
			    <title>PASSWORD RESET</title>
			    <link href="https://fonts.googleapis.com/css?family=Expletus+Sans" rel="stylesheet" type="text/css">
			    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
			</head>

			<body style="margin:0px; padding:0px; font-family: Open Sans, Tahoma, Times, serif; background: rgb(77, 158, 185) none repeat scroll 0% 0%; width: 100%; float: left;">
			    <div class="container" style="width:590px; margin:auto;margin-top:50px;margin-bottom:50px;">
			        <div class="container1" style="background: #fff;width: 100%;float: left;margin-bottom:50px;">
			            <div class="cont" style="width: 490px;float: left;text-align: center;margin: 25px 0px 0px 43px;">
			                <img src="http://immtradersclub.com/images/logo.png" style="margin:0 0 20px 0;width:200px;  "><br/><br/>
			                <div class="header" style="font-weight: 600;color: rgb(255, 255, 255);font-size: 30px;
			line-height: 30px;padding: 18px 0px 12px;background-color: rgb(255, 114, 67); font-family: Arial, cursive;">
			                   Password Reset Confirmation
			                </div>
			                <div class="pay-head" style="font-family: Lato;font-weight: 400;color: rgb(72, 72, 72);font-size: 25px;line-height: 35px; margin-top: 13px;">
			                    Dear '.$emails.',
			                </div>
			                <div class="border" style="width: 500px;text-align: left;height: 1px;background-color: #000;float: left;">
			                </div>
			                <div class="txt" style="font-family: Lato,Arial;font-weight: 400;font-size: 15px;line-height: 23px;
			color: rgb(38, 38, 38);width: 100%;margin-top: 24px;">
			                    <p style="margin: 0px !important;">PASSWORD RESET VERIFICATION</p>
			                </div>
			                <div class="amount" style="color: rgb(72, 72, 72);line-height: 35px;font-family: Lato;">


			                     <h4>PLEASE CLICK ON THE LINK BELOW TO RESET YOUR PASSWORD.</h4>

			                       <h4>Secure Login URL: https://immtradersclub.com/member/login </h4>
			                 
			                   	<h3>Reset Link :</h3>
			                    <a href="https://immtradersclub.com/member/reset/password/'.$code.'" style="margin: 8px 0px 10px !important;font-weight: 300;font-size: 20px"> https://immtradersclub.com/member/reset/password/'.$code.'/'.$emails.'</a>
			                   
			         
			                    <h3>ACCOUNT NOTIFICATIONS</h3>
			                    <p>To ensure that you receive all our notifications, we recommend that you give your valid email address and

			check your email on regular basis.</p>
			<h3>NEED ASSISTANCE?</h3>
			<p>If you have any further questions please feel free leave comments on contact us.</p>
			                   <p> Thank you,<br/>Interday Markets Management.<br/>Operation Dept.</p>
			                  </div>
			                <div class="line" style="height: 1px;background: rgb(218, 218, 218) none repeat scroll 0% 0%;margin-top: 20px;">		               
			                </div>
			                <p style="font-family: Lato, Arial; font-weight: 400; font-size: 15px; line-height: 24px; color: #0c0b0c; -webkit-font-smoothing: antialiased; margin: 26px 0px 0px !important;">
			                  Copyrights 2016 Immtradersclub. All Rights Reserved. </p>
			                
			            </div>
			        </div>
			    </div>
			    </div><br/><br/>
			</body>

			</html>';

	
		 	$this->load->library("phpmailer_library");
		    $mail = $this->phpmailer_library->load();

			$mail->SMTPDebug = 4; // Enable verbose debug output
				$mail->isSMTP();
				$mail->SMTPAutoTLS = false;
				$mail->SMTPOptions = array(
									    'ssl' => array(
									        'verify_peer' => false,
									        'verify_peer_name' => false,
									        'allow_self_signed' => true
									    )
									);
				$mail->SMTPSecure = 'ssl'; 
				$mail->SMTPAuth = true; 
				$mail->Host = 'ssl://smtp.gmail.com'; 
				$mail->Username = 'cyberspace418@gmail.com'; 
				$mail->Password = 'Louise2)!&'; 
				$mail->Port = 465;
				$mail->SetLanguage("tr", "phpmailer/language");
				$mail->CharSet ="utf-8";
				$mail->Encoding="base64";
				$mail->SMTPDebug = false;
				$mail->do_debug = 0;
				$mail->setFrom('cyberspace418@gmail.com', 'IMM TRADERS');
				$mail->addAddress($emails,$emails); 
				$mail->isHTML(true); 
				$mail->Subject = "PASSWORD RESET";
				$mail->Body = $msg;

				$this->session->set_userdata(['myemail'=>$emails]);
			
				if($this->model_users->count_ref(['email'=>$emails]) > 0 ){
						if(!$mail->send()) {
							//$this->session->set_userdata(['success_msg'=>''])

							 array_push($data,[

					 					'title' 	=>'Oops !',
					 					'msg'		=> 'Something went wrong',
					 					'status'	=>'error'
					 				]);
							}
					else {

						//echo json_encode('success');

						$this->model_users->update(['passwd_recovery_code'=>$code],['email'=>$emails]);
						array_push($data,[

					 					'title' 	=>'Good Job !',
					 					'msg'		=> 'Reset Link has been sent to your email',
					 					'status'	=>'success'
					 				]);
					}
				}else {


					 array_push($data,[

					 					'title' 	=>'Oops !',
					 					'msg'		=> 'Email Not Found in our record',
					 					'status'	=>'error'
					 				]);
				}

			echo json_encode($data);

	}



	public function confirmCode(){

			$code =$this->uri->segment(3);
			$generated = $this->session->userdata('reset_code');

			$data = [

					'email' =>$this->uri->segment(4)
					];



			if($code == $generated){
				redirect('reset/password/confirm/confirms');
			}
			else {

					echo 'Link Expire';
			}

		}

	public function resetView(){
			$this->load->view('users/confirm_reset');

	}

	public function resetPassword () {

			$data = [];

			$password 		 = $this->input->post('password');
			$confirmpassword = $this->input->post('confirmpassword');
		
			$email = $this->session->userdata('myemail') ;
				 					

			if($password == $confirmpassword){


				if($this->model_users->update(['passwd'=>$this->authentication->hash_passwd($password)],['email'=>$email])){

						array_push($data,[

				 					'title' 	=>'Good Job !',
				 					'msg'		=> 'Password has been updated. Got to Login page and login in',
				 					'status'	=>'success'
				 				]);
				}
				else {

						array_push($data,[

				 					'title' 	=>'Oops !',
				 					'msg'		=> 'Something went wrong',
				 					'status'	=>'error'
				 				]);

				}

			}
			else {

					array_push($data,[

				 					'title' 	=>'Oops !',
				 					'msg'		=> 'Password Mismatch',
				 					'status'	=>'error'
				 				]);

			}
		echo json_encode($data);
	}

	public function searchUser(){

			

			$user = $this->input->post('user');
			$data = [] ;
			$email = "" ;
				
			if($user !="") {

				if($this->model_users->query("Select email,first_name,last_name,user_id from user_registration where user_id='$user' OR email='$user' ")->result()) {

						foreach ($this->model_users->query("Select email,first_name,last_name,user_id from user_registration where user_id='$user' OR email='$user' ")->result() as $key => $value) {
							$email = $value->email;
							
							array_push($data,
											[
												'email' 		=>	$value->email,
												'user_id' 		=>	$value->user_id,
												'name' 			=> 	$value->first_name . ' ' .$value->last_name,
											]
									);
						}
				}
				else {
								array_push($data,
													[
														'email' => 'Sponsor not found !',
														'name' 	=> 	'',
														'user_id' => ''
													]
										);

				}


			}
			else {

					array_push($data,
													[
														'email' => 'Please enter your sponsor !',
														'name' 	=> 	'',
														'user_id' => ''
													]
										);


			}
				echo json_encode($data);			
	}



	public function create_user() {

				$imm_token 			= $this->input->post('imm_token');
				$phoneNumber 		= $this->input->post('phoneNumber');
				$defaultCountry 	= $this->input->post('defaultCountry');
				$carrierCode 		= $this->input->post('carrierCode');
				$sponsor 			= $this->input->post('sponsor');
				$email 				= $this->input->post('email');

				$this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');
                $this->load->helper('auth');
				$this->load->model('examples/examples_model');
				$this->load->model('examples/validation_callables');


				$data = [] ;
				$sponsor_id 		= '';

                $passwd = $this->authentication->hash_passwd($this->generateRandomString());

                $user_data = [	
								'passwd'     => $passwd,
								'email'      => $email,
								'telephone'  => $phoneNumber,
								'auth_level' => '1', 
							];
				$this->form_validation->set_data( $user_data );


					$validation_rules = [
								[
									'field' => 'telephone',
									'label' => 'telephone',
									'rules' => 'max_length[12]|is_unique[' . db_table('user_table') . '.telephone]',
									'errors' => [
										'is_unique' => 'PhoneNumber already in use.'
									]
								],
								[
									'field' => 'passwd',
									'label' => 'passwd',
									'rules' => [
										'trim',
										'required',
										[ 
											'_check_password_strength', 
											[ $this->validation_callables, '_check_password_strength' ] 
										]
									],
									'errors' => [
										'required' => 'The password field is required.'
									]
								],
								[
									'field'  => 'email',
									'label'  => 'email',
									'rules'  => 'trim|required|valid_email|is_unique[' . db_table('user_table') . '.email]',
									'errors' => [
										'is_unique' => 'Email address already in use.'
									]
								],
								[
									'field' => 'auth_level',
									'label' => 'auth_level',
									'rules' => 'required|integer|in_list[1,6,9]'
								]
						];
					
				$this->form_validation->set_rules( $validation_rules );

				if( $this->form_validation->run()  ) {
						
					foreach ($this->model_users->query("Select user_id from user_registration where user_id='$sponsor' OR email='$sponsor'")->result() as $key => $value) {
											
					}
						
				}
				
				
			return $this->load->view('users/register');


	}

	public function generateRandomString($length = 10) {
		    $characters 				= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength 			= strlen($characters);
		    $randomPass 				= '';

		    for ($i = 0; $i < $length; $i++) {
		        $randomPass .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomPass;
}


}
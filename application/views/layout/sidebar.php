 <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=site_url()?>dashboard">Dashboard</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-group"></i> Member Managament <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=site_url()?>member/list">Member List</a></li>
                      <li><a href="media_gallery.html">Password Tracker</a></li>
                      <li><a href="<?=site_url()?>member/announcement">Official Anouncement</a></li> 
                      <li><a href="media_gallery.html">Sub Admin Management</a></li>
                      <li><a href="<?=site_url()?>member/daily-profit">Daily Profit Share</a></li>
                      <!--
                      <li><a href="media_gallery.html">Bitcoin Pending Payments</a></li>-->
                      <li><a href="<?=site_url()?>member/close-account">Close Account List</a></li>
                      <li><a href="<?=site_url()?>member/fund-requests">Fund Requests</a></li>
                     <!-- <li><a href="media_gallery.html">Approved Fund Requests</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-file-pdf-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="media_gallery.html">Direct Income Report</a></li>
                      <li><a href="media_gallery.html">Profit Share Income Report</a></li>
                      <li><a href="typography.html">Unilevel Income Report</a></li>
                      <li><a href="icons.html">Direct Member Report</a></li>
                      <li><a href="glyphicons.html">Unilevel Downline Report</a></li>
                      <li><a href="widgets.html">Users Downline Purchase Report</a></li>
                      <li><a href="invoice.html">Users Bitcoin Payment Report</a></li>
                      <li><a href="inbox.html">Coin Converted Report</a></li>
                      <li><a href="calendar.html">All Report Managemnet</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-google-wallet"></i> E-Wallet Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=site_url()?>wallet/manage-ewallet">Manage User E-Wallet</a></li>
                      <li><a href="tables_dynamic.html">Manage User Coins</a></li>
                      <li><a href="<?=site_url()?>wallet/manage-rwallet">Manage User R-Wallet</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-database"></i> Coin Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Buy History</a></li>
                      <li><a href="chartjs2.html">Sell History</a></li>
                      <li><a href="morisjs.html">Bankwire Request for Coin</a></li>
                      <li><a href="echarts.html">IMC percentage update</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cc-mastercard"></i> Withdrawals <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Open Withdrawal Request</a></li>
                      <li><a href="chartjs2.html">Close Withdrawal Request</a></li>
                      <li><a href="morisjs.html">Manage Withdrawals</a></li>
                      <li><a href="echarts.html">Search Withdrawals</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i>Member Tree <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Direct Member Tree</a></li>
                
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cog"></i>Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Password Management</a></li>
                      <li><a href="fixed_sidebar.html">Profile Photo Management</a></li>
                      <li><a href="fixed_sidebar.html">User Emails</a></li>
                
                    </ul>
                  </li>
                   <li><a><i class="fa fa-tasks"></i>Royalty Bonus <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Add Royalty Bonus</a></li>
                      <li><a href="fixed_sidebar.html">Rising Star</a></li>
                      <li><a href="fixed_sidebar.html">Flying Star</a></li>
                      <li><a href="fixed_sidebar.html">Champion</a></li>
                      <li><a href="fixed_sidebar.html">Elite</a></li>
                      <li><a href="fixed_sidebar.html">Co-Founder</a></li>
                
                    </ul>
                  </li>
                </ul>
              </div>
             

            </div>
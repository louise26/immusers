<?php $this->view('layout/body_header') ?>


    
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->view('layout/menu_profile')?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
           <?php $this->view('layout/sidebar')?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->view('layout/menu_footer')?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php $this->view('layout/top_nav')?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">

            <div class="col-md-6 col-xs-12">
                  <div class="x_panel">
                  <div class="x_title">
                    <h2>GENERAL INFORMATION<small></small></h2>
                  
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                   

                    <form class="form-horizontal form-label-left" >

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Sponsor ID</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <?php
                                $sponsor_id = "" ;
                                $spnosr_name = "";

                              foreach ($sponsor as $key => $sponsor) {

                                  $sponsor_id = $sponsor->user_id;
                                  $spnosr_name = $sponsor->first_name;

                              }
                              foreach ($member_profile as $key => $profile) {}

                            ?>
                          <input type="text" class="form-control" placeholder="" value="<?=$sponsor_id ?>" readonly="">
                        </div>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Sponsor Name</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" placeholder="" readonly="" value="<?=$spnosr_name?>">
                          </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User Registration Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" readonly="" value="<?=date('F d, Y',strtotime($profile->registration_date))?>">
                        </div>
                      </div>
                   
                    </form>
                  </div>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>MEMBER PROFILE <small>PESRSONAL INFORMATION</small></h2>
                  
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                   

                    <form class="form-horizontal form-label-left" name="profileForm">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Userid</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" readonly="" value="<?=$profile->user_id?>" name="user_id">
                        </div>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" placeholder="" value="<?=$profile->username?>" name="username">
                          </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Firstname</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->first_name?>" name="first_name">
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Lastname</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->last_name?>" name="last_name">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Number</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->telephone?>" name="telephone">
                        </div>
                      </div>
                    
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <textarea class="form-control" rows="3" placeholder="Address" value="<?=$profile->address?>" name="address"><?=$profile->address?></textarea>
                        </div>
                      </div>
                
                   
                   
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="select2_single form-control" tabindex="-1" name="country">
                            <option value="<?=$profile->country?>"><?=$profile->country?></option>
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>
                            <option value="CA">California</option>
                            <option value="NV">Nevada</option>
                            <option value="OR">Oregon</option>
                            <option value="WA">Washington</option>
                            <option value="AZ">Arizona</option>
                            <option value="CO">Colorado</option>
                            <option value="ID">Idaho</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NM">New Mexico</option>
                            <option value="ND">North Dakota</option>
                            <option value="UT">Utah</option>
                            <option value="WY">Wyoming</option>
                            <option value="AR">Arkansas</option>
                            <option value="IL">Illinois</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="OK">Oklahoma</option>
                            <option value="SD">South Dakota</option>
                            <option value="TX">Texas</option>
                          </select>
                        </div>
                      </div>
                  
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->state?>" name="state">
                        </div>
                      </div>
                 
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->city?>" name="city">
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="password" class="form-control" placeholder="" value="<?=$profile->passwd?>" name="passwd">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Transaction Password</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="password" class="form-control" placeholder="" value="<?=$profile->t_code?>" name="t_code">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->dob;?>" name="dob">
                        </div>
                      </div>
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="select2_single form-control" tabindex="-1"  name="sex">
                            <option value="<?=$profile->sex?>"><?=$profile->sex?></option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            
                          </select>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                           <a href="<?=site_url()?>member/list" class="btn btn-success">Cancel</a>
                          <button type="submit" id="btn_profile" class="btn btn-success">Update</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>


              <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small>BANK INFORMATION</small></h2>
                  
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                   

                    <form class="form-horizontal form-label-left" name="bankForm">
                      <input type="hidden" class="form-control" placeholder="" readonly="" value="<?=$profile->user_id?>" name="user_id">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->acc_name?>" name="acc_name">
                        </div>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Number</label>
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" class="form-control" placeholder="" value="<?=$profile->ac_no?>" name="ac_no">
                          </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->bank_nm?>" name="bank_nm">
                        </div>
                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branche Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->branch_nm?>" name="branch_nm">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ifsc/Swift Code</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->swift_code?>" name="swift_code">
                        </div>
                      </div>
  
                  
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bitcoin</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->bitcoin?>" name="bitcoin">
                        </div>
                      </div>
                 
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ethereum</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->ethereum?>" name="bitcoin">
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ripple</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->ripple?>" name="ripple">
                        </div>
                      </div>

                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ripple Destination Tag</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->ripple_tag?>" name="ripple_tag">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Ethereum Classic</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="" value="<?=$profile->ethereumc?>" name="ethereumc">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <a href="<?=site_url()?>member/list" class="btn btn-success">Cancel</a>
                          <button type="submit" class="btn btn-success" id="btn_bank">Update</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php $this->view('layout/footer') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php $this->view('layout/scripts') ?>


     <script src="<?=base_url()?>assets/admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

    <script src="<?=base_url()?>assets/js/jquery-confirm.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>
   
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/profile.js"></script>

    <script>
       $(document).ready(function () {
               $('.ui-pnotify').remove();
        });
    </script>


<?php $this->view('layout/body_footer')?>
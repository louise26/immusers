<?php $this->view('layout/body_header') ?>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <?php $this->view('layout/menu_profile')?>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
           <?php $this->view('layout/sidebar')?>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <?php $this->view('layout/menu_footer')?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php $this->view('layout/top_nav')?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">


                 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>CLOSE ACCOUNT REQUESTS <small>List</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <p class="text-muted font-13 m-b-30">
                        
                    </p>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        
                    </p>
                    <table id="datatable-button" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Userid</th>
                          <th>Username</th>
                          <th>Member Name</th>
                          <th> Request Date</th>
                          <th>Amount</th>
                          <th>Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        </tr>    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">


                 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>CLOSED ACCOUNTS  <small>History</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <p class="text-muted font-13 m-b-30">
                        
                    </p>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        
                    </p>
                    <table  id="myTable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                        <th>#</th>
                          <th>Userid</th>
                          <th>Username</th>
                          <th>Member Name</th>
                          <th> Request Date</th>
                          <th>Amount</th>
                          <th>Status</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                        </tr>    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->view('layout/footer') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php $this->view('layout/scripts') ?>


     <script src="<?=base_url()?>assets/admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
     <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>

     <script src="<?=base_url()?>assets/js/jquery-confirm.js"></script>

    <script>
       $(document).ready(function () {
               $('.ui-pnotify').remove();
        });
    </script>
    <script type="text/javascript" src="<?=base_url()?>assets/admin/js/account.js">   </script>




<?php $this->view('layout/body_footer')?>
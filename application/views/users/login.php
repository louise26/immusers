<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Login</title>
    <!-- Bootstrap -->
    <link href="<?=base_url()?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url()?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=base_url()?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?=base_url()?>assets/admin/vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?=base_url()?>assets/admin/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
             <?php
                         if( ! isset( $on_hold_message ) )
                                        {
                                            if( isset( $login_error_mesg ) )
                                            {
                                                echo '
                                                    <div style="border:1px solid red;">
                                                        <p>
                                                            Login Error #' . $this->authentication->login_errors_count . '/' . config_item('max_allowed_attempts') . ': Invalid Username, Email Address, or Password.
                                                        </p>
                                                        <p>
                                                            Username, email address and password are all case sensitive.
                                                        </p>
                                                    </div>
                                                ';
                                            }
                                            if( $this->input->get(AUTH_LOGOUT_PARAM) )
                                            {
                                                echo '
                                                    <div style="border:1px solid green">
                                                        <p>
                                                            You have successfully logged out.
                                                        </p>
                                                    </div>
                                                ';
                                            }
                         echo form_open( $login_url, ['class' => 'form-horizontal m-t-20','id'=>'submit'] ); 
                         ?>
            <form>
              <h1>Admin | Login Form</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required=""  name="login_string" id="login_string"/>
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="login_pass" id="login_pass" required="" <?php 
                                    if( config_item('max_chars_for_password') > 0 )
                                        echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
                                ?> />
              </div>
              <div>
                <button class="btn btn-default submit" type="submit" id="submit_button">Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
              

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> iMM-Traders</h1>
                  <p>©2018 All Rights Reserved. iMM-Traders Club.</p>
                </div>
              </div>
            </form>
            <?php     
                     }
                     else
                        {
                                // EXCESSIVE LOGIN ATTEMPTS ERROR MESSAGE
                                echo '
                                    <div style="border:1px solid red;">
                                        <p>
                                            Excessive Login Attempts
                                        </p>
                                        <p>
                                            You have exceeded the maximum number of failed login<br />
                                            attempts that this website will allow.
                                        <p>
                                        <p>
                                            Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
                                        </p>
                                        <p>
                                            Please use the <a href="/examples/recover">Account Recovery</a> after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
                                            or contact us if you require assistance gaining access to your account.
                                        </p>
                                    </div>
                                ';
                            }
                            ?>
          </section>
        </div>

   
      </div>
    </div>
  </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="iMM-Traders" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta property="og:site_name" content="iMM-Traders">
        <meta property="og:site" content="https://immtradersclub.com/member/">
        <meta property="og:title" content="iMM-Traders">
        <meta property="og:description" content="Learn Invest Earn">
        <meta property="og:image" content="https://immtradersclub.com/member/assets/images/flogo.png">
        <meta property="og:url" content="https://immtradersclub.com/member/"><meta property="og:type" content="article">
        <meta name="description" content="Learn Invest Earn">
        <meta name="keywords" content="imm,imc , immcoin,trading,cryptocurrency, digital cash, digital money, coin, crypto coin">
        <title>iMM-Traders | <?= strtoupper($this->uri->segment(1))?> - <?=strtoupper($this->uri->segment(2))?></title>
        <link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="https://immtradersclub.com//assets/plugins/assets/images/favicon.ico">
        <link href="<?=base_url()?>assets/plugins/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css">
         <link href="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?=base_url()?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?=base_url()?>assets/plugins/morris/morris.css">
        
       
    </head>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
                        <?php $this->view('users/topnav'); ?>
                         <?php $this->view('users/sidebar'); ?>


            </div>
<?php $this->view('layout/body_header') ?>



            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <?php $this->view('layout/menu_profile')?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
           <?php $this->view('layout/sidebar')?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <?php $this->view('layout/menu_footer')?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php $this->view('layout/top_nav')?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">


                 <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ADD FUND FOR  USERID: <?=$this->uri->segment(4)?><small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-role" name="formAdd">
                         <div class="form-group">
                          <label>Current Balance :</label>
                          <label id="balance">  <?=$balance?></label>
                         </div>
                         <div class="form-group">
                          <label> USER ID :</label>
                            <input type="text" name="user_id" value="<?=$this->uri->segment(4)?>" readonly="" class="form-control">
                         </div>
                          <div class="form-group">
                          <label> AMOUNT TO ADD :</label>
                            <input type="number" name="amount" step="any"  class="form-control" required="">
                         </div>
                         <div class="form-group">
                          <label> REMARK :</label>
                              <select name="remark" class="form-control" id="remark" required="">
                                                <option value="">Select One</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="NETS">NETS</option>
                                                <option value="VISA/MASTER">VISA/MASTER</option>
                                                <option value="CASH">CASH</option>
                                                <option value="OTHERS">OTHERS</option>
                                            </select>
                         </div>
                         <div class="form-group">
                               <button type="submit" class="btn btn-primary" id="btn_add">Submit</button>
                         </div>
                    </form>
                     
                    
              
                  </div>
                </div>
              </div>

                <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DEDUCT FUND FOR  USERID: <?=$this->uri->segment(4)?><small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                         <form class="form-role" name="formDeduct">
                         <div class="form-group">
                         <label>Current Balance :</label>
                         <label id="balances">  <?=$balance?></label>
                         </div>
                         <div class="form-group">
                          <label> USER ID :</label>
                            <input type="text" name="userid" value="<?=$this->uri->segment(4)?>" readonly="" class="form-control">
                         </div>
                          <div class="form-group">
                          <label> AMOUNT TO DEDUCT :</label>
                            <input type="number" name="amounts" step="any"  class="form-control">
                         </div>
                         <div class="form-group">
                          <label> REMARK :</label>
                               <input type="text" name="remarks"  class="form-control">
                         </div>
                         <div class="form-group">
                               <button type="submit" id="btn_deduct" class="btn btn-primary">Submit</button>
                         </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php $this->view('layout/footer') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php $this->view('layout/scripts') ?>


     <script src="<?=base_url()?>assets/admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>

    <script src="<?=base_url()?>assets/js/jquery-confirm.js"></script>

    <script>
       $(document).ready(function () {
               $('.ui-pnotify').remove();
        });
    </script>
    <script src="<?=base_url()?>assets/pages/rwallet.js"></script>

    <?php $this->view('layout/body_footer')?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[LOGIN_PAGE] 								= 'examples/login';
$route['default_controller'] 					= 'examples';


// D A S H S E C T I O N
$route['dashboard'] 							= 'users/dashboard';
$route['member/list'] 							= 'memberlistcontroller';
$route['member/getlist'] 						= 'memberlistcontroller/getList';
$route['member/activate']						= 'memberlistcontroller/activateUser';
$route['member/deactivate']						= 'memberlistcontroller/deactivateUser';


$route['member/edit/:num']						= 'memberlistcontroller/editUser';
$route['member/updateinfo']						= 'memberlistcontroller/updateinfo';
$route['member/updatebank']						= 'memberlistcontroller/updatebank';

$route['member/announcement']					= 'announcementcontroller';
$route['member/updateannouncement']				= 'announcementcontroller/updateannouncement';
$route['member/clearannouncement']				= 'announcementcontroller/clearannouncement';

$route['member/daily-profit']					= 'profitsharecontroller';
$route['member/getprofits'] 					= 'profitsharecontroller/getProfits';
$route['member/add-profit'] 					= 'profitsharecontroller/addProfit';
$route['member/close-account'] 					= 'accountcontroller';
$route['member/get-request'] 					= 'accountcontroller/getrequest';
$route['member/approve-request'] 				= 'accountcontroller/approveRequest';
$route['member/cancel-request'] 				= 'accountcontroller/cancelRequest';
$route['member/get-close'] 						= 'accountcontroller/getclose';

$route['member/fund-requests'] 					= 'fundrequestcontroller';
$route['member/get-fundrequest'] 				= 'fundrequestcontroller/getrequest';
$route['member/get-approvefundrequest'] 		= 'fundrequestcontroller/getclose';

//$route['member/fund-requests'] 					= 'fundrequestcontroller';
//$route['member/get-fundrequest'] 				= 'fundrequestcontroller/getrequest';
$route['member/cancelled-request'] 				= 'fundrequestcontroller/cancelRequest';
$route['member/paid-request'] 					= 'fundrequestcontroller/approveRequest';

$route['wallet/manage-rwallet'] 				= 'walletcontroller';
$route['wallet/getlist'] 						= 'walletcontroller/getList';
$route['wallet/manage-rwallet/admin-ts/:any'] 	= 'walletcontroller/admints';
$route['wallet/admints/:any'] 					= 'walletcontroller/getadmints';
$route['wallet/manage-rwallet/manage/:any']		= 'walletcontroller/managewallet';
$route['wallet/addfund'] 						= 'walletcontroller/updateRwallet';
$route['wallet/deductfund'] 					= 'walletcontroller/deductRwallet';

$route['wallet/getewalletlist'] 				= 'walletcontroller/getewalletList';
$route['wallet/manage-ewallet'] 				= 'walletcontroller/ewallet';


$route['royalty/flyingstar']						= 'flyingstarcontroller';










$route['reset/password/:any']					= 'users/register/confirmCode';
$route['reset/password/confirm/confirms']		= 'users/register/resetView';
$route['reset/password/confirm/confirms/reset']	= 'users/register/resetPassword';

$route['password/recovery']				='examples/recover';
$route['password/recovery_verification/:any/:any']				='examples/recovery_verification';



$route['404_override']			= '';
$route['translate_uri_dashes'] 	= FALSE;
